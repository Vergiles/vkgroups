﻿using Android.Accounts;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Preferences;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;

namespace VKGroups
{
    [Activity(Label = "Группы ВК", 
        MainLauncher = true, 
        LaunchMode = LaunchMode.SingleTop, 
        Icon = "@mipmap/icon")]
    public class HomeMenu : BaseActivity
    {
        private ISharedPreferences sharedPrefs;
        private DrawerActionBarToggle drawerToggle;
        private DrawerLayout drawerLayout;
        private ListView drawerListView;
        private readonly string prefUserToken = "UserToken";
        private string drawerTitle;
        private string title; // Заголовок страницы
        public static string UserAccessToken = ""; // Токен пользователя
        private readonly string[] sections = { "Поиск", "Отслеживаемые", "Выход" };

        protected override int LayoutResource => Resource.Layout.menupage;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            GetDeviceUserAccoutVK();

            title = drawerTitle = Title;

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawerListView = FindViewById<ListView>(Resource.Id.left_drawer);

            var adapter = new ArrayAdapter<string>(
                    this, 
                    Resource.Layout.cellmenu, 
                    sections
            );

            drawerListView.Adapter = adapter;
            drawerListView.ItemClick += ItemTappedEventHandler;
            drawerLayout.SetDrawerShadow(
                    Resource.Mipmap.drawer_shadow_dark, 
                    (int)GravityFlags.Start
            );

            drawerToggle = new DrawerActionBarToggle(
                    this, 
                    drawerLayout, 
                    Toolbar,
                    Resource.String.drawer_open, 
                    Resource.String.drawer_close
            );

            drawerToggle.DrawerClosed += DrawerClosedEventHandler;
            drawerToggle.DrawerOpened += DrawerOpenedEventHandler;

#pragma warning disable CS0618 // Тип или член устарел
            drawerLayout.SetDrawerListener(drawerToggle);
#pragma warning restore CS0618 // Тип или член устарел
        }

        /// <summary>
        /// Gets the device user accout vk. - Запрос на получение данных
        /// аккаунта Vkontakte с устройства
        /// </summary>
        private void GetDeviceUserAccoutVK() 
        {
            sharedPrefs = PreferenceManager.GetDefaultSharedPreferences(this);
            var accountManager = AccountManager.Get(this);

            Account[] accounts = accountManager.GetAccountsByType("com.vkontakte.account");

            if (accounts.Length != 0)
                StartActivityForResult(
                    NetworkService.InitializerOAuthVK(), 
                    1); // Вызываем авторизацию VK, с последующим результатом
            else
                Toast.MakeText(
                    this, 
                    GetString(Resource.String.no_oauth), 
                    ToastLength.Long)
                    .Show(); // Показываем сообщение о том что авторизация не удалась
        }

        /// <summary>
        /// Items the tapped event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void ItemTappedEventHandler(object sender, AdapterView.ItemClickEventArgs e) => 
            ItemClickedEventHandler(e.Position);

        /// <summary>
        /// Drawers the opened event handler.
        /// </summary>
        /// <param name="s">S.</param>
        /// <param name="e">E.</param>
        private void DrawerOpenedEventHandler(object s, ActionBarDrawerEventArgs e) 
        {
            SupportActionBar.Title = drawerTitle;
            InvalidateOptionsMenu();
        }

        /// <summary>
        /// Drawers the closed event handler.
        /// </summary>
        /// <param name="s">S.</param>
        /// <param name="e">E.</param>
        private void DrawerClosedEventHandler(object s, ActionBarDrawerEventArgs e)
        {
            SupportActionBar.Title = title;
            InvalidateOptionsMenu();
        }

        /// <summary>
        /// Items the clicked event handler. - нажатие на ячейку из левого 
        /// бокового меню   
        /// </summary>
        /// <param name="position">Position.</param>
        private void ItemClickedEventHandler(int position)
        {           
            Android.Support.V4.App.Fragment fragment = null;

            switch (position)
            {
                case 0:
                    fragment = new FSearchGroup();
                    break;
                case 1:
                    fragment = new FTrackedGroup();
                    break;
                case 2:
                    RemoveAllDataUserApp();
                    break;
            }

            if (fragment != null)
            {
                SupportFragmentManager.BeginTransaction()
                        .Replace(Resource.Id.content_frame, fragment)
                        .Commit();

                drawerListView.SetItemChecked(position, true);
                SupportActionBar.Title = title = sections[position];
                drawerLayout.CloseDrawers();
            }
        }

        public override bool OnPrepareOptionsMenu(IMenu menu)
        {
            var drawerOpen = drawerLayout.IsDrawerOpen((int)GravityFlags.Left);

            for (int i = 0; i < menu.Size(); i++)
                menu.GetItem(i).SetVisible(!drawerOpen);

            return base.OnPrepareOptionsMenu(menu);
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            drawerToggle.SyncState();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            drawerToggle.OnConfigurationChanged(newConfig);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) => 
            drawerToggle.OnOptionsItemSelected(item) || base.OnOptionsItemSelected(item);
            
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 1 && data != null && data.HasExtra("access_token"))
            {
                string accessToken = data.GetStringExtra("access_token");
                System.Diagnostics.Debug.WriteLine(accessToken);
                if(!string.IsNullOrEmpty(accessToken))
                    WriteSettingUser(accessToken);
            }
        }

        /// <summary>
        /// Writes the setting user. Запись настройки пользователя
        /// </summary>
        /// <param name="getToken">Get token.</param>
        private void WriteSettingUser(string getToken) 
        {
            ISharedPreferencesEditor edit = sharedPrefs.Edit();
            edit.PutString(prefUserToken, getToken);
            edit.Commit();

            UserAccessToken = getToken;

            ItemClickedEventHandler(1);
        }

        /// <summary>
        /// Removes all data user app. Удаление всех данных пользователя из приложения
        /// </summary>
        private void RemoveAllDataUserApp() 
        {
            ISharedPreferencesEditor edit = sharedPrefs.Edit();
            edit.Clear().Commit();
            UserAccessToken = "";

            SQLiteDatabase.RemoveAllData(GetString(Resource.String.name_current_groups));
            SQLiteDatabase.RemoveAllData(GetString(Resource.String.name_search_groups));
            Toast.MakeText(this, GetString(Resource.String.data_user_delete), ToastLength.Long).Show();
        }
    }
}