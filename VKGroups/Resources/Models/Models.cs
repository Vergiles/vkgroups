﻿using Newtonsoft.Json;
using SQLite;
using System.Collections.Generic;

namespace VKGroups
{
    /*
     * <summary> 
     * Контейнер классов с атрибутами
     * </summary>           
     */
    public class Models
    {
        #region Models My Get And Search Groups
        /*
         * <summary> 
         * Классы: ItemCurrentGroups, ModelsCurrentGroups, ResponseGetCurrentGroups
         * Которые возврощают ответ от сервера по текущим группам пользователя        
         * </summary>            
         */
        public class ItemCurrentGroups
        {
            [JsonProperty(PropertyName = "id")]
            public int Id { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "screen_name")]
            public string Screen_name { get; set; }
            [JsonProperty(PropertyName = "is_closed")]
            public int Is_closed { get; set; }
            [JsonProperty(PropertyName = "type")]
            public string Type { get; set; }
            [JsonProperty(PropertyName = "is_admin")]
            public int Is_admin { get; set; }
            [JsonProperty(PropertyName = "is_member")]
            public int Is_member { get; set; }
            [JsonProperty(PropertyName = "is_advertiser")]
            public int Is_advertiser { get; set; }
            [JsonProperty(PropertyName = "photo_50")]
            public string Photo_50 { get; set; }
            [JsonProperty(PropertyName = "photo_100")]
            public string Photo_100 { get; set; }
            [JsonProperty(PropertyName = "photo_200")]
            public string Photo_200 { get; set; }
            [JsonProperty(PropertyName = "admin_level")]
            public int? Admin_level { get; set; }
            [JsonProperty(PropertyName = "deactivated")]
            public string Deactivated { get; set; }
        }

        public class ModelsCurrentGroups
        {
            [JsonProperty(PropertyName = "count")]
            public int Count { get; set; }
            [JsonProperty(PropertyName = "items")]
            public List<ItemCurrentGroups> Items { get; set; }
        }

        public class ResponseGetCurrentGroups
        {
            [JsonProperty(PropertyName = "response")]
            public ModelsCurrentGroups Response { get; set; }
        }
        #endregion

        #region Models Leave Groups
        /*
         * <summary> 
         * Возврощает ответ по удалению пользователя из группы      
         * </summary>            
         */
        public class ResponseLeaveGroups
        {
            [JsonProperty(PropertyName = "response")]
            public int Response { get; set; }
        }
        #endregion

        #region Models Current Group
        /*
         * <summary> 
         * Классы: ItemOnlyCurrentGroup, ResponseOnlyCurrentGroup
         * Которые возврощают ответ от сервера по конкретной группе        
         * </summary>            
         */
        public class ItemOnlyCurrentGroup
        {
            [JsonProperty(PropertyName = "id")]
            public int Id { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "screen_name")]
            public string Screen_name { get; set; }
            [JsonProperty(PropertyName = "is_closed")]
            public int Is_closed { get; set; }
            [JsonProperty(PropertyName = "type")]
            public string Type { get; set; }
            [JsonProperty(PropertyName = "is_admin")]
            public int Is_admin { get; set; }
            [JsonProperty(PropertyName = "is_member")]
            public int Is_member { get; set; }
            [JsonProperty(PropertyName = "is_advertiser")]
            public int Is_advertiser { get; set; }
            [JsonProperty(PropertyName = "description")]
            public string Description { get; set; }
            [JsonProperty(PropertyName = "photo_50")]
            public string Photo_50 { get; set; }
            [JsonProperty(PropertyName = "photo_100")]
            public string Photo_100 { get; set; }
            [JsonProperty(PropertyName = "photo_200")]
            public string Photo_200 { get; set; }
        }

        public class ResponseOnlyCurrentGroup
        {
            [JsonProperty(PropertyName = "response")]
            public List<ItemOnlyCurrentGroup> Response { get; set; }
        }
        #endregion
    }
}
