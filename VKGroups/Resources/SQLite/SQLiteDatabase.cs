﻿using System;
using System.Collections.Generic;
using SQLite;
using System.IO;
using static VKGroups.Models;

namespace VKGroups
{
    /// <summary>
    /// SQLite database. - Работа с базой данных
    /// </summary>
    public static class SQLiteDatabase
    {
        private static string folder = Environment.GetFolderPath(
            Environment.SpecialFolder.Personal);


        /// <summary>
        /// Creates the group data base. - Создание таблицы 
        /// </summary>
        /// <returns><c>true</c>, if group data base was created, 
        /// <c>false</c> otherwise.</returns>
        /// <param name="nameDB">Name db.</param>
        public static bool CreateGroupDataBase(string nameDB)
        {
            try
            {
                using (var connectionTableGroups = new SQLiteConnection(
                    Path.Combine(folder, nameDB + ".db")))
                {
                    connectionTableGroups.CreateTable<ItemCurrentGroups>();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Inserts the table groups. - Добавление данных в таблицу
        /// </summary>
        /// <returns>The table groups.</returns>
        /// <param name="nameDB">Name db.</param>
        /// <param name="soureListGroups">Soure list groups.</param>
        public static List<ItemCurrentGroups> InsertTableGroups(string nameDB, 
                List<ItemCurrentGroups> soureListGroups)
        {
            try
            {
                using (var connectionTableGroups = new SQLiteConnection(
                    Path.Combine(folder, nameDB + ".db")))
                {
                    if (SelectTableGroups(nameDB).Count == 0)
                        foreach (var item in soureListGroups)
                            connectionTableGroups.Insert(item);
                    else
                    {
                        if (RemoveAllData(nameDB))
                            foreach (var item in soureListGroups)
                                connectionTableGroups.Insert(item);
                    }

                    return soureListGroups;
                }
            }
            catch
            {
                return new List<ItemCurrentGroups>();
            }
        }

        /// <summary>
        /// Selects the table groups. - Получение данных из базы
        /// </summary>
        /// <returns>The table groups.</returns>
        /// <param name="nameDB">Name db.</param>
        public static List<ItemCurrentGroups> SelectTableGroups(string nameDB)
        {
            try
            {
                using (var connectionTableGroups = new SQLiteConnection(
                    Path.Combine(folder, nameDB + ".db")))
                    return connectionTableGroups.Table<ItemCurrentGroups>().ToList();
            }
            catch
            {
                return new List<ItemCurrentGroups>();
            }
        }

        /// <summary>
        /// Removes the table groups. - Удаление конкретных данных из таблицы
        /// </summary>
        /// <returns><c>true</c>, if table groups was removed, <c>false</c> 
        /// otherwise.</returns>
        /// <param name="nameDB">Name db.</param>
        /// <param name="soureListGroups">Soure list groups.</param>
        public static bool RemoveTableGroups(string nameDB, ItemCurrentGroups soureListGroups)
        {
            try
            {
                using (var connectionTableGroups = new SQLiteConnection(
                    Path.Combine(folder, nameDB + ".db")))
                {
                    connectionTableGroups.Delete(soureListGroups);
                    return true; 
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Removes all data. - Полное удаление всех данных из таблиц
        /// </summary>
        /// <returns><c>true</c>, if all data was removed, <c>false</c> 
        /// otherwise.</returns>
        /// <param name="nameDB">Name db.</param>
        public static bool RemoveAllData(string nameDB)
        {
            try
            {
                using (var connectionTableGroups = new SQLiteConnection(
                    Path.Combine(folder, nameDB + ".db")))
                {
                    connectionTableGroups.DeleteAll<ItemCurrentGroups>();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}