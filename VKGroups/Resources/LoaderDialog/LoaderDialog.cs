﻿using Android.App;
using Android.Content;

namespace VKGroups
{
    /// <summary>
    /// Loader dialog. Анимация загрузки
    /// </summary>
    public class LoaderDialog : ProgressDialog
    {
        public LoaderDialog(Context context) : base(context)
        {
            Indeterminate = true;
            SetProgressStyle(ProgressDialogStyle.Spinner);
            SetCancelable(false);
        }

        /// <summary>
        /// Shows the loader. -  Показать окно загрузки
        /// </summary>
        /// <param name="message">Message.</param>
        public void ShowLoader(string message)
        {
            SetMessage(message);
            Show();
        }
    }
}
