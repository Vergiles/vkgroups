﻿using System;
using System.Collections.Generic;
using Android.OS;
using Android.Views;

namespace VKGroups
{
    /// <summary>
    /// FT racked group. - Страница "Отслеживаемых групп"
    /// </summary>
    public class FTrackedGroup : BaseGroup
    {
        public FTrackedGroup()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater,
                ViewGroup container,
                Bundle savedInstanceState)
        {
           var view = onCreateBaseView(
                inflater, container, savedInstanceState,
                Resource.String.name_current_groups,
                Resource.Layout.pagetrackedgroup,
                Resource.Id.listView
            );

            GetGroups();
            return view;
        }

        /// <summary>
        /// Gets the groups. - запрашивает данные с сервера по текущим группам,
        ///  в которых состоит пользователь
        /// </summary>
        private async void GetGroups()
        {
            try
            {
                progressLoader.ShowLoader(GetString(
                    Resource.String.message_loader_groups));

                if (!NetworkService.StatusWiFi())
                    throw new Exception(GetString(Resource.String.off_wifi));

                if (!NetworkService.StatusUserAccessToken())
                    throw new Exception(GetString(Resource.String.no_oauth));

                var getGroups = await NetworkService.ApiService()
                    .GetCurrentGroups(HomeMenu.UserAccessToken);

                System.Diagnostics.Debug.WriteLine("getGroups count : " + getGroups.Response.Count);

                sourceGroups = SQLiteDatabase.InsertTableGroups(
                    GetString(Resource.String.name_current_groups),
                    getGroups.Response.Items
                );

                System.Diagnostics.Debug.WriteLine("sourceGroups count : " + sourceGroups.Count);

                listViewGroups.Adapter = new IGroupsAdatpar(this, sourceGroups);

                progressLoader.Hide();

            }
            catch (Exception ex)
            {
                sourceGroups = SQLiteDatabase.SelectTableGroups(
                    GetString(Resource.String.name_current_groups));

                if (sourceGroups.Count != 0)
                    listViewGroups.Adapter = new IGroupsAdatpar(this, sourceGroups);

                progressLoader.Hide();
                AlertDisplay(ex.Message);
            }
        }
    }
}