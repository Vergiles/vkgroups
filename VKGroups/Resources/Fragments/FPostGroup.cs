﻿using System;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;

namespace VKGroups
{
    /*
     * <summary> 
     * Окно страницы "Информация о конкретной группе"
     * </summary>    
     */
    public class FPostGroup : Fragment
    {
        private View view;
        private ImageViewAsync avatart;
        private TextView title;
        private TextView description;
        private LoaderDialog progressLoader;
        private string idGroup;

        public FPostGroup(Models.ItemCurrentGroups item)
        {
            this.idGroup = item.Id.ToString();
            this.RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, 
            ViewGroup container, 
            Android.OS.Bundle savedInstanceState)
        {
            this.HasOptionsMenu = true;

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.pagepostgroup, null);

            avatart = view.FindViewById<ImageViewAsync>(Resource.Id.avaGroup);
            title = view.FindViewById<TextView>(Resource.Id.nameGroup);
            description = view.FindViewById<TextView>(Resource.Id.descriptionGroup);

            progressLoader = new LoaderDialog(Context);

            GetPostGroups();

            return view;
        }

        /// <summary>
        /// Gets the post groups. - запрашивает данные с сервера по конкретной группе
        /// </summary>
        private async void GetPostGroups()
        {
            try
            {
                progressLoader.ShowLoader(GetString(
                    Resource.String.message_loader_only_current_group));

                if (!NetworkService.StatusWiFi())
                    throw new Exception(GetString(Resource.String.off_wifi));

                if (!NetworkService.StatusUserAccessToken())
                    throw new Exception(GetString(Resource.String.no_oauth));

                var result = await NetworkService.ApiService()
                    .GetOnlyCurrentGroup(HomeMenu.UserAccessToken, idGroup);

                var response = result.Response[0];
                ImageService.Instance.LoadUrl(response.Photo_200).Into(avatart);
                title.Text = response.Name;
                description.Text = response.Description;

                progressLoader.Hide();
            }
            catch (Exception ex)
            {
                progressLoader.Hide();
                Toast.MakeText(view.Context, ex.Message, ToastLength.Long).Show();
                FragmentManager.BeginTransaction().Replace(
                       Resource.Id.content_frame,
                       new FTrackedGroup()
               ).Commit();
            }
        }
    }
}
