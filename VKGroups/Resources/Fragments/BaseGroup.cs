﻿using System;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Collections.Generic;
using static Android.Resource;

namespace VKGroups
{
    /// <summary>
    /// BaseGroup
    /// </summary>
    public class BaseGroup : Fragment
    {
        private View view;
        public LoaderDialog progressLoader;
        public ListView listViewGroups;

        public List<Models.ItemCurrentGroups> sourceGroups;

        public BaseGroup()
        {
            RetainInstance = true;
        }

        public View onCreateBaseView(
            LayoutInflater inflater, ViewGroup container, Android.OS.Bundle savedInstanceState,
            int groupDataBase, int pageGroup, int searchView
        )
        {
            HasOptionsMenu = true;

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var createGroupDataBase = SQLiteDatabase.CreateGroupDataBase(
                GetString(groupDataBase));

            System.Diagnostics.Debug.WriteLine("CreateGroupDataBase bool : " + createGroupDataBase);

            view = inflater.Inflate(pageGroup, null);

            progressLoader = new LoaderDialog(Context);

            listViewGroups = view.FindViewById<ListView>(searchView);
            listViewGroups.ItemClick += ItemTappedEventHandler;

            return view;
        }

        /// <summary>
        /// Items the tapped event handler. - нажатие на ячейку из списка    
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        private void ItemTappedEventHandler(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (sourceGroups != null)
                FragmentManager.BeginTransaction().Replace(
                        Resource.Id.content_frame,
                        new FPostGroup(sourceGroups[e.Position])
                ).Commit();
        }

        public void AlertDisplay(string Message) => Toast.MakeText(
            view.Context, 
            Message, 
            ToastLength.Long)
            .Show();
    };
}