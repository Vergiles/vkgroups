﻿using System;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Collections.Generic;

namespace VKGroups
{
    /// <summary>
    /// FS earch group. - Страница "Поиск групп"
    /// </summary>
    public class FSearchGroup : BaseGroup
    {
        private List<Models.ItemCurrentGroups> sourceSearchGroups;

        public FSearchGroup()
        {
            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater,
            ViewGroup container,
            Android.OS.Bundle savedInstanceState)
        {
            var view = onCreateBaseView(
                inflater, container, savedInstanceState,
                Resource.String.name_search_groups,
                Resource.Layout.pagesearchgroup,
                Resource.Id.lVSearchGroups
            );

            var searchViewGroups = view.FindViewById<SearchView>(Resource.Id.sGroup);
            int id = searchViewGroups.Context.Resources.GetIdentifier(
                    "android:id/search_src_text",
                    null,
                    null
            );
            searchViewGroups.QueryTextChange += SearchTextEventHandler;

            var textViewSearch = (TextView)searchViewGroups.FindViewById(id);
            textViewSearch.SetTextColor(Color.Black);
            textViewSearch.Hint = GetString(Resource.String.entry_name_group);
            textViewSearch.SetHintTextColor(Color.White);

            return view;
        }

        /// <summary>
        /// Searchs the text event handler. ввода событий в поисковой строке  
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="arg">Argument.</param>
        private void SearchTextEventHandler(object sender, SearchView.QueryTextChangeEventArgs arg)
        {
            if (!string.IsNullOrEmpty(arg.NewText))
                GetSearchGroups(arg.NewText);
            else
            {
                SQLiteDatabase.RemoveAllData(GetString(Resource.String.name_search_groups));
                listViewGroups.Adapter = null;
            }
        }

        /// <summary>
        /// Gets the search groups. - Запрашивает данные с сервера по всем группам
        /// </summary>
        /// <param name="text">Text.</param>
        private async void GetSearchGroups(string text)
        {
            try
            {
                progressLoader.ShowLoader(GetString(
                    Resource.String.message_search_group));

                if (!NetworkService.StatusWiFi())
                    throw new Exception(GetString(Resource.String.off_wifi));

                if (!NetworkService.StatusUserAccessToken())
                    throw new Exception(GetString(Resource.String.no_oauth));

                var getSearchGroups = await NetworkService.ApiService()
                    .GetSearchGroups(HomeMenu.UserAccessToken, text);

                sourceSearchGroups = getSearchGroups.Response.Items;

                listViewGroups.Adapter = new ISearchGroupsAdatpar(
                        SQLiteDatabase.InsertTableGroups(
                            GetString(Resource.String.name_search_groups),
                            sourceSearchGroups)
                );

                progressLoader.Hide();

            }
            catch (Exception ex)
            {
                sourceSearchGroups = SQLiteDatabase.SelectTableGroups(
                    GetString(Resource.String.name_search_groups));

                if (sourceSearchGroups.Count != 0)
                    listViewGroups.Adapter = new ISearchGroupsAdatpar(
                            SQLiteDatabase.SelectTableGroups(
                                GetString(Resource.String.name_search_groups))
                    );

                AlertDisplay(ex.Message);
            }
        }
    };
}