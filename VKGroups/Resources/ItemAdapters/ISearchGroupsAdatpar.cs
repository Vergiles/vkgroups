﻿using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using static VKGroups.Models;

namespace VKGroups
{
    public class ViewSearchGroupHolder : Java.Lang.Object
    {
        public ImageViewAsync Photo { get; set; }
        public TextView Title { get; set; }
        public TextView CountPost { get; set; }
    }

    public class ISearchGroupsAdatpar : BaseAdapter<ItemCurrentGroups>
    {
        private List<ItemCurrentGroups> sourceSearchGroups;

        public ISearchGroupsAdatpar(List<ItemCurrentGroups> sourceSearchGroups)
        {
            this.sourceSearchGroups = sourceSearchGroups;
        }

        public override ItemCurrentGroups this[int position] => sourceSearchGroups[position];

        public override int Count => sourceSearchGroups.Count;

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
            {
                view = LayoutInflater.From(parent.Context).Inflate(
                    Resource.Layout.cellgroup, 
                    parent, 
                    false
                );

                var photo = view.FindViewById<ImageViewAsync>(Resource.Id.photoImageView);
                var name = view.FindViewById<TextView>(Resource.Id.nameTextView);
                var department = view.FindViewById<TextView>(Resource.Id.departmentTextView);
                var more = view.FindViewById<ImageView>(Resource.Id.moreGMenu);
                more.Visibility = ViewStates.Gone;

                view.Tag = new ViewHolder { 
                    Photo = photo, 
                    Title = name, 
                    CountPost = department
                };
            }

            var holder = (ViewHolder)view.Tag;

            ImageService.Instance.LoadUrl(sourceSearchGroups[position].Photo_100)
                .Into(holder.Photo);

            holder.Title.Text = sourceSearchGroups[position].Name;
            holder.CountPost.Text = string.Format(
                "Постов загружено: {0}", 
                sourceSearchGroups.Count);

            return view;
        }
    }
}
