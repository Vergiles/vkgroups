﻿using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using FFImageLoading;
using FFImageLoading.Views;
using static VKGroups.Models;

namespace VKGroups
{
    public class ViewHolder : Java.Lang.Object
    {
        public ImageViewAsync Photo { get; set; }
        public TextView Title { get; set; }
        public TextView CountPost { get; set; }
        public ImageView More { get; set; }
    }

    public class IGroupsAdatpar : BaseAdapter<ItemCurrentGroups>
    {
        private BaseGroup fragTrackedGroup;
        private List<ItemCurrentGroups> sourceGroups;

        public IGroupsAdatpar(BaseGroup fragTrackedGroup, List<ItemCurrentGroups> sourceGroups)
        {
            this.fragTrackedGroup = fragTrackedGroup;
            this.sourceGroups = sourceGroups;
        }

        public override ItemCurrentGroups this[int position] => sourceGroups[position];

        public override int Count => sourceGroups.Count;

        public override long GetItemId(int position) => position;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
            {
                view = LayoutInflater.From(parent.Context).Inflate(
                    Resource.Layout.cellgroup,
                    parent,
                    false
                );

                var photo = view.FindViewById<ImageViewAsync>(Resource.Id.photoImageView);
                var name = view.FindViewById<TextView>(Resource.Id.nameTextView);
                var department = view.FindViewById<TextView>(Resource.Id.departmentTextView);
                var more = view.FindViewById<ImageView>(Resource.Id.moreGMenu);

                view.Tag = new ViewHolder
                {
                    Photo = photo,
                    Title = name,
                    CountPost = department,
                    More = more
                };
            }

            var holder = (ViewHolder)view.Tag;

            ImageService.Instance.LoadUrl(sourceGroups[position].Photo_100)
                .Into(holder.Photo);

            holder.Title.Text = sourceGroups[position].Name;
            holder.CountPost.Text = string.Format(
                "Постов загружено: {0}",
                sourceGroups.Count);

            holder.More.SetImageResource(Resource.Drawable.more);
            holder.More.Id = position;

            if (!holder.More.HasOnClickListeners)
                holder.More.Click += delegate {
                    ShowMenu(parent.Context, view, sourceGroups[holder.More.Id]);
                };

            return view;
        }

        /*
         * <summary> 
         * Показать всплывающее меню
         * </summary>    
         * <param name="view">Вид колонки</param> 
         * <param name="item">Данные о группе</param>   
         */
        private void ShowMenu(Context context, View view, ItemCurrentGroups itemGroups)
        {
            PopupMenu menu = new PopupMenu(context, view, GravityFlags.Right);

            menu.MenuInflater.Inflate(Resource.Menu.moregroup, menu.Menu);
            menu.MenuItemClick += (object s, PopupMenu.MenuItemClickEventArgs arg) =>
            {
                EventsButtonPopupMenu(view, arg.Item.TitleFormatted.ToString(), itemGroups);
            };

            menu.Show();
        }

        /*
         * <summary> 
         * Событие нажатой кнопки в всплывающем меню
         * </summary>    
         * <param name="view">Вид колонки</param> 
         * <param name="title">Текст нажатой кнопки</param>   
         * <param name="item">Данные о группе</param>   
         */
        private void EventsButtonPopupMenu(View view, string title, ItemCurrentGroups item)
        {
            switch (title)
            {
                case "Перезагрузить все посты":
                    if (SQLiteDatabase.RemoveAllData(fragTrackedGroup
                        .GetString(Resource.String.name_current_groups)))
                    {
                        sourceGroups.Clear();
                        NotifyDataSetChanged();
                        RefreshAllGroup(view);
                    }
                    break;
                case "Удалить группу":
                    DeleteGroup(view, item);
                    break;
            }
        }

        /*
         * <summary> 
         * Обвновление списка "Отслеживаемых групп"
         * </summary>    
         * <param name="view">Вид колонки</param> 
         */
        private async void RefreshAllGroup(View view)
        {
            try
            {
                fragTrackedGroup.progressLoader.ShowLoader(
                    fragTrackedGroup.GetString(
                        Resource.String.message_loader_update_group)
                );

                if (!NetworkService.StatusWiFi())
                    throw new System.Exception(fragTrackedGroup.GetString(
                        Resource.String.off_wifi));

                var resulte = await NetworkService.ApiService()
                    .GetCurrentGroups(HomeMenu.UserAccessToken);

                fragTrackedGroup.sourceGroups = sourceGroups = SQLiteDatabase.InsertTableGroups(
                    fragTrackedGroup.GetString(Resource.String.name_current_groups),
                    resulte.Response.Items);

                fragTrackedGroup.progressLoader.Hide();
                NotifyDataSetChanged();

            }
            catch (System.Exception ex)
            {
                fragTrackedGroup.progressLoader.Hide();
                Toast.MakeText(view.Context, ex.Message, ToastLength.Long).Show();
            }
        }

        /*
         * <summary> 
         * Удаление пользователя из группы
         * </summary>    
         * <param name="view">Вид колонки</param> 
         * <param name="item">Данные о группе</param>         
         */
        private async void DeleteGroup(View view, ItemCurrentGroups item)
        {
            try
            {
                fragTrackedGroup.progressLoader.ShowLoader(
                    fragTrackedGroup.GetString(
                        Resource.String.message_loader_exit_group)
                );

                if (!NetworkService.StatusWiFi())
                    throw new System.Exception(fragTrackedGroup.GetString(Resource.String.off_wifi));

                var result = await NetworkService.ApiService()
                .LeaveGroups(HomeMenu.UserAccessToken, item.Id.ToString());

                if (result.Response == 1)
                {
                    var removeTableGrous = SQLiteDatabase.RemoveTableGroups(
                        fragTrackedGroup.GetString(Resource.String.name_current_groups),
                        item);

                    sourceGroups.Remove(item);
                    fragTrackedGroup.progressLoader.Hide();
                    NotifyDataSetChanged();

                }
            }
            catch (System.Exception ex)
            {
                fragTrackedGroup.progressLoader.Hide();
                Toast.MakeText(view.Context, ex.Message, ToastLength.Long).Show();
            }
        }
    }
}
