﻿using System.Threading.Tasks;
using Android.Content;
using Plugin.Connectivity;
using Refit;

namespace VKGroups
{
    /// <summary>
    /// Network service. - Работа c сетевым сервисом
    /// </summary>
    public static class NetworkService
    {
        /// <summary>
        /// Statuses the wi fi. - Статус WiFi 
        /// </summary>
        /// <returns><c>true</c>, if wi fi was statused, <c>false</c> 
        /// otherwise.</returns>
        public static bool StatusWiFi() => CrossConnectivity.Current.IsConnected;

        /// <summary>
        /// Statuses the user access token. - Статус токена пользователя 
        /// </summary>
        /// <returns><c>true</c>, if user access token was statused, 
        /// <c>false</c> otherwise.</returns>
        public static bool StatusUserAccessToken() => 
            !string.IsNullOrEmpty(HomeMenu.UserAccessToken);

        /// <summary>
        /// Statuses the host. - Статус сервера 
        /// </summary>
        /// <returns>The host.</returns>
        public static async Task<bool> StatusHost() => 
            await CrossConnectivity.Current.IsRemoteReachable("https://vk.com/");

        /// <summary>
        /// Initializers the OA uth vk. Авторизация пользователя в ВК 
        /// </summary>
        /// <returns>The OA uth vk.</returns>
        public static Intent InitializerOAuthVK() => new Intent(
            "com.vkontakte.android.action.SDK_AUTH", 
            null)
            .PutExtra("version", "5.92") // версия
            .PutExtra("client_id", 6973030) // id приложения
            .PutExtra("scope", "groups"); // права доступа к группам

      
        /// <summary>
        /// APIs the service. - API Сервис
        /// </summary>
        /// <returns>The service.</returns>
        public static IApiService ApiService() => RestService.For<IApiService>(
            "https://api.vk.com");
    }
}
