﻿using System.Threading.Tasks;
using Refit;
using static VKGroups.Models;

namespace VKGroups
{
    /// <summary>
    /// API service. - Интерфейс по запросам на сервер   
    /// </summary>
    [Headers("Accept: application/json")]
    public interface IApiService
    {
        /// <summary>
        /// Gets the current groups. - Запрос по всем отслеживаемых групп
        /// </summary>
        /// <returns>The current groups.</returns>
        /// <param name="token">Token.</param>
        [Get("/method/groups.get?access_token={tokenUser}&v=5.92&extended=1")]
        Task<ResponseGetCurrentGroups> GetCurrentGroups(string tokenUser);

        /// <summary>
        /// Gets the search groups. - Запрос по нахождению групп
        /// </summary>
        /// <returns>The search groups.</returns>
        /// <param name="tokenUser">Token user.</param>
        /// <param name="nameGroup">Name group.</param>
        [Get("/method/groups.search?access_token={tokenUser}&q={nameGroup}&v=5.92")]
        Task<ResponseGetCurrentGroups> GetSearchGroups(string tokenUser, string nameGroup = null);

        /// <summary>
        /// Gets the only current group. - Запрос по конкретной группе
        /// </summary>
        /// <returns>The only current group.</returns>
        /// <param name="token">Token.</param>
        /// <param name="group_id">Group identifier.</param>
        [Get("/method/groups.getById?access_token={tokenUser}&group_ids={groupId}&fields=description&v=5.92")]
        Task<ResponseOnlyCurrentGroup> GetOnlyCurrentGroup(string tokenUser, string groupId);

        /// <summary>
        /// Leaves the groups. -  Запрос по удалению пользователя из группы
        /// </summary>
        /// <returns>The groups.</returns>
        /// <param name="tokenUser">Token user.</param>
        /// <param name="groupId">Group identifier.</param>
        [Get("/method/groups.leave?access_token={tokenUser}&group_id={groupId}&v=5.92")]
        Task<ResponseLeaveGroups> LeaveGroups(string tokenUser, string groupId);
    }
}
